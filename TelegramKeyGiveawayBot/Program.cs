﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text.Json;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;
using Telegram.Bot;
using Telegram.Bot.Exceptions;
using Telegram.Bot.Types;
using Telegram.Bot.Types.Enums;
using Telegram.Bot.Types.InlineQueryResults;
using Telegram.Bot.Types.ReplyMarkups;
using File = System.IO.File;

namespace TelegramKeyGiveawayBot;

internal static class Program {
	private static TelegramBotClient BotClient;
	private static readonly SemaphoreSlim FileSemaphore = new(1, 1);
	private static ConcurrentDictionary<string, GiveawayInfo> Giveaways = new();
	internal static readonly Random Random = new();
	private static readonly SemaphoreSlim ShutdownSemaphore = new(0, 1);
	private static readonly Dictionary<long, (SemaphoreSlim WaitSemaphore, Message ResultMessage)> WaitMessageStatuses = new();

	private static readonly SemaphoreSlim GroupSendMessageSemaphore = new(1, 1);
	private static readonly SemaphoreSlim SendMessageSemaphore = new(1, 1);

	private static readonly Regex KeyRegex = new("[0-9A-Z]{4,7}-[0-9A-Z]{4,7}-[0-9A-Z]{4,7}(?:(?:-[0-9A-Z]{4,7})?(?:-[0-9A-Z]{4,7}))?",
		RegexOptions.Compiled | RegexOptions.CultureInvariant);

	private static async Task Main() {
		Logger.Info("Starting " + nameof(TelegramKeyGiveawayBot));
		var token = await File.ReadAllTextAsync("token.txt").ConfigureAwait(false);

		if (File.Exists("data.json")) {
			await using var file = File.OpenRead("data.json");
			Giveaways = await JsonSerializer.DeserializeAsync<ConcurrentDictionary<string, GiveawayInfo>>(file).ConfigureAwait(false);
		}

		BotClient = new TelegramBotClient(token);
		BotClient.StartReceiving(HandleUpdate, HandleError);
		await ShutdownSemaphore.WaitAsync().ConfigureAwait(false);
	}

	private static Task HandleError(ITelegramBotClient client, Exception exception, CancellationToken cancellationToken)
	{
		return Task.CompletedTask;
	}

	private static Task HandleUpdate(ITelegramBotClient client, Update update, CancellationToken cancellationToken)
	{
		return update.Type switch
		{
			UpdateType.Message => HandleMessage(update.Message!),
			UpdateType.CallbackQuery => HandleCallbackQuery(update.CallbackQuery!),
			UpdateType.InlineQuery => HandleInlineQuery(update.InlineQuery!),
			UpdateType.ChosenInlineResult => HandleInlineResultChosen(update.ChosenInlineResult!)
		};
	}

	private static async Task HandleCallbackQuery(CallbackQuery query) {
		string giveawayID = query.Data;
		var userName = NameUserRetrieve(query.From);
		Logger.Info($"Got callback {giveawayID} from {userName}");

		if (!Giveaways.ContainsKey(giveawayID)) {
			await AnswerCallbackQueryAsync(query.Id, "Данной раздачи не существует!").ConfigureAwait(false);
			return;
		}

		Logger.Debug($"Entering {giveawayID} semaphore...");
		await Giveaways[giveawayID].GiveawaySemaphore.WaitAsync().ConfigureAwait(false);
		Logger.Debug($"Entered {giveawayID} semaphore!");

		int keysCount;
		try {
			if (!Giveaways.ContainsKey(giveawayID)) {
				await AnswerCallbackQueryAsync(query.Id, "Данной раздачи не существует!").ConfigureAwait(false);
				Logger.Debug($"{giveawayID}|{userName}: Doesn't exist");
				return;
			}

			if (Giveaways[giveawayID].AvailableKeys.Count == 0) {
				await AnswerCallbackQueryAsync(query.Id, "Раздача завершена!").ConfigureAwait(false);
				Logger.Debug($"{giveawayID}|{userName}: Finished");
				return;
			}

			if (Giveaways[giveawayID].ParticipatedUsers.Contains(query.From.Id)) {
				await AnswerCallbackQueryAsync(query.Id, "Вы уже участвовали в раздаче!").ConfigureAwait(false);
				Logger.Debug($"{giveawayID}|{userName}: Already participated");
				return;
			}

			var key = Giveaways[giveawayID].AvailableKeys.GetRandomValue();
			Logger.Debug($"{giveawayID}|{userName}: Sending key");
			await SendTextMessageAsync(query.From.Id, $"Ваш ключ: `{key}`", parseMode: ParseMode.Markdown).ConfigureAwait(false);
			Giveaways[giveawayID].ParticipatedUsers.Add(query.From.Id);
			Logger.Info($"Sent key {key} from giveaway {giveawayID} to {userName}");
			Giveaways[giveawayID].AvailableKeys.Remove(key);

			keysCount = Giveaways[giveawayID].AvailableKeys.Count;
			var title = Giveaways[giveawayID].Title;
			Logger.Debug($"{giveawayID}|{userName}: Editing");
			await EditMessageTextAsync(Giveaways[giveawayID].MessageInlineID,
					(string.IsNullOrEmpty(title) ? "" : title + "\n") +
					(keysCount > 0
						? $"Раздача {keysCount} ключей!\nДля получения нажмите на кнопку ниже."
						: "Раздача закончена!"),
					keysCount > 0
						? new InlineKeyboardMarkup(new InlineKeyboardButton("Получить ключ!") {
							CallbackData = query.Data
						})
						: null, keysCount == 0)
				.ConfigureAwait(false);

			Logger.Debug($"{giveawayID}: Editing done");
		} catch (Exception ex) {
			try {
				await AnswerCallbackQueryAsync(query.Id, "Произошла неизвестная ошибка, попробуйте ещё раз!").ConfigureAwait(false);
			} catch (ApiRequestException) {
				// ignored
			}

			Logger.Error($"{giveawayID}|{userName}: {ex}");
		} finally {
			Logger.Debug($"Exiting {giveawayID} semaphore...");
			Giveaways[giveawayID].GiveawaySemaphore.Release();
			Logger.Debug($"Exited {giveawayID} semaphore!");
		}

		keysCount = Giveaways[giveawayID].AvailableKeys.Count;
		if (keysCount == 0) {
			Logger.Debug("Entering semaphore to dispose it...");
			await Giveaways[giveawayID].GiveawaySemaphore.WaitAsync().ConfigureAwait(false);
			Giveaways[giveawayID].GiveawaySemaphore.Release();
			Giveaways[giveawayID].GiveawaySemaphore.Dispose();
			Logger.Debug($"Disposed {giveawayID} semaphore!");
			Giveaways.TryRemove(giveawayID, out _);
		}

		await FileSemaphore.WaitAsync().ConfigureAwait(false);

		try {
			await using var file = File.Open("data.json", FileMode.Create);
			await JsonSerializer.SerializeAsync(file, Giveaways).ConfigureAwait(false);
		} catch (Exception ex) {
			Logger.Debug(ex.ToString());
		} finally {
			FileSemaphore.Release();
		}

		try {
			await AnswerCallbackQueryAsync(query.Id, "Ключ был успешно выслан!").ConfigureAwait(false);
		} catch (ApiRequestException) {
			// ignored
		}
	}

	private static async Task HandleInlineQuery(InlineQuery inlineQuery) {
		Logger.Info($"Got inline query \"{inlineQuery.Query}\" from {NameUserRetrieve(inlineQuery.From)}");
		if (string.IsNullOrEmpty(inlineQuery.Query)) {
			var giveawaysFromUser = Giveaways.Where(giveaway => giveaway.Value.OwnerID == inlineQuery.From.Id).ToList();
			if (!giveawaysFromUser.Any()) {
				await BotClient.AnswerInlineQueryAsync(inlineQuery.Id, Array.Empty<InlineQueryResult>()).ConfigureAwait(false);
			}

			await BotClient.AnswerInlineQueryAsync(inlineQuery.Id,
				giveawaysFromUser.Select(giveaway => new InlineQueryResultArticle(giveaway.Key,
					$"{giveaway.Value.Title ?? "Без названия"}: {giveaway.Value.AvailableKeys.Count} ключей", new InputTextMessageContent($"{giveaway.Value.Title}\nРаздача {giveaway.Value.AvailableKeys.Count} ключей!\nДля получения нажмите на кнопку ниже.")) {
					ReplyMarkup = new InlineKeyboardMarkup(new InlineKeyboardButton("Получить ключ!") {
						CallbackData = giveaway.Key
					})
				})).ConfigureAwait(false);

			return;
		}

		if (Giveaways.Count(giveaway => giveaway.Value.OwnerID == inlineQuery.From.Id) >= 30) {
			try {
				await BotClient.AnswerInlineQueryAsync(inlineQuery.Id, Array.Empty<InlineQueryResult>()).ConfigureAwait(false);
			} catch (ApiRequestException) {
				// ignored
			}
		}

		var matches = KeyRegex.Matches(inlineQuery.Query);
		if (matches.Count < 1) {
			try {
				await BotClient.AnswerInlineQueryAsync(inlineQuery.Id, Array.Empty<InlineQueryResult>()).ConfigureAwait(false);
			} catch (ApiRequestException) {
				// ignored
			}

			return;
		}

		var keys = matches.Select(match => match.Value).Distinct().ToHashSet();
		try {
			var id = RandomString(32);
			await BotClient.AnswerInlineQueryAsync(inlineQuery.Id,
				new[] {
					new InlineQueryResultArticle(id, $"Создать раздачу {keys.Count} ключей", new InputTextMessageContent($"Раздача {keys.Count} ключей!\nДля получения нажмите на кнопку ниже.")) {
						ReplyMarkup = new InlineKeyboardMarkup(new InlineKeyboardButton("Получить ключ!") {
							CallbackData = id
						})
					}
				}).ConfigureAwait(false);
		} catch (ApiRequestException) {
			// ignored
		}
	}

	private static async Task HandleInlineResultChosen(ChosenInlineResult chosenInlineResult) {
		Logger.Info($"Got inline result {chosenInlineResult.InlineMessageId}|{chosenInlineResult.ResultId} chosen by {NameUserRetrieve(chosenInlineResult.From)}");
		if (!Giveaways.ContainsKey(chosenInlineResult.ResultId)) {
			if (!Giveaways.TryAdd(chosenInlineResult.ResultId, new GiveawayInfo(KeyRegex.Matches(chosenInlineResult.Query).Select(match => match.Value).Distinct().ToHashSet(), chosenInlineResult.From.Id))) {
				try {
					await SendTextMessageAsync(chosenInlineResult.From.Id, "Не удалось создать раздачу! Попробуйте ещё раз.").ConfigureAwait(false);
				} catch (ApiRequestException) {
					// ignored
				}

				return;
			}
		}

		Giveaways[chosenInlineResult.ResultId].MessageInlineID = chosenInlineResult.InlineMessageId;

		await FileSemaphore.WaitAsync().ConfigureAwait(false);

		try {
			await using var file = File.OpenWrite("data.json");
			await JsonSerializer.SerializeAsync(file, Giveaways).ConfigureAwait(false);
		} catch (Exception ex) {
			Logger.Debug(ex.ToString());
		} finally {
			FileSemaphore.Release();
		}
	}

	private static async Task HandleMessage(Message message) {
		Logger.Info($"Got message \"{message.Text ?? message.Caption}\" from {NameUserRetrieve(message.From)}");
		if (WaitMessageStatuses.ContainsKey(message.From.Id)) {
			if (message.Text?.ToUpperInvariant() != "/ABORT") {
				WaitMessageStatuses[message.From.Id] = (WaitMessageStatuses[message.From.Id].WaitSemaphore, message);
			}

			if (WaitMessageStatuses[message.From.Id].WaitSemaphore.CurrentCount == 0) {
				WaitMessageStatuses[message.From.Id].WaitSemaphore.Release();
			}

			return;
		}

		if ((message.Chat.Type != ChatType.Private) || (message.Type != MessageType.Text) || string.IsNullOrEmpty(message.Text) || (message.Text[0] != '/')) {
			return;
		}

		var messageText = message.Text;
		var arguments = messageText.ToUpperInvariant().Split(' ', StringSplitOptions.RemoveEmptyEntries);
		if (arguments[0].Contains('@')) {
			arguments[0] = arguments[0].Substring(0, arguments[0].IndexOf('@'));
		}

		arguments[0] = arguments[0].Substring(1);
		switch (arguments[0]) {
			case "DONATE":
				await SendTextMessageAsync(message.Chat, "Спасибо, что решили задонатить!\n" +
					"WebMoney:\n  WMP: P254203748668\n  WMR: R981120646412\n  WMZ: Z028553335407\n" +
					"[ЮMoney](https://yoomoney.ru/to/410012842075676)\n" +
					"[Ссылка обмена Steam](https://steamcommunity.com/tradeoffer/new/?partner=186729617&token=XvcQ5RHm)\n" +
					"[Связаться с разработчиком](https://t.me/ezhevita)", parseMode: ParseMode.Markdown).ConfigureAwait(false);
				return;
			case "CREATE":
				if (Giveaways.Count(giveaway => giveaway.Value.OwnerID == message.From.Id) >= 30) {
					await SendTextMessageAsync(message.Chat, "Вы превысили лимит по раздачам. Пожалуйста, удалите или дождитесь завершения одной из них.").ConfigureAwait(false);
				}

				await SendTextMessageAsync(message.Chat, "Вы решили создать раздачу, отлично! Введите название раздачи:").ConfigureAwait(false);
				var giveawayNameMessage = await WaitForMessage(message.From.Id).ConfigureAwait(false);
				if (giveawayNameMessage is not {Type: MessageType.Text} || string.IsNullOrEmpty(giveawayNameMessage.Text)) {
					await SendTextMessageAsync(message.Chat, "Операция отменена.").ConfigureAwait(false);
					return;
				}

				await SendTextMessageAsync(message.Chat, "Теперь вводите ключи Steam для раздачи. Внимание - лимит длины одного сообщения равен 4096 символам, учитывайте это!" +
					" Когда закончите вводить ключи - напишите /finish.").ConfigureAwait(false);

				HashSet<string> keys = [];
				while (true) {
					var keyMessage = await WaitForMessage(message.From.Id).ConfigureAwait(false);
					if ((keyMessage == null) || (keyMessage.Type != MessageType.Text) || string.IsNullOrEmpty(keyMessage.Text)) {
						await SendTextMessageAsync(message.Chat, "Операция отменена.").ConfigureAwait(false);
						return;
					}

					if (keyMessage.Text.ToUpperInvariant() == "/FINISH") {
						if (keys.Count == 0) {
							await SendTextMessageAsync(message.Chat, "Ключей не были добавлены, операция отменена.").ConfigureAwait(false);
							return;
						}

						break;
					}

					var keysOldCount = keys.Count;
					var matches = KeyRegex.Matches(keyMessage.Text);
					if (keys.Count + matches.Count > 1000) {
						await SendTextMessageAsync(message.Chat, "Общее количество ключей превысит 1000 штук, пожалуйста, уменьшите их количество или разбейте раздачу на несколько.").ConfigureAwait(false);
						continue;
					}

					keys.UnionWith(matches.Select(match => match.Value).Distinct());

					await SendTextMessageAsync(message.Chat, $"Добавлено {keys.Count - keysOldCount} ключей").ConfigureAwait(false);
				}

				if (Giveaways.TryAdd(RandomString(32), new GiveawayInfo(keys, message.From.Id, giveawayNameMessage.Text.Length > 50 ? giveawayNameMessage.Text.Substring(0, 50) : giveawayNameMessage.Text))) {
					await SendTextMessageAsync(message.Chat, "Раздача успешно создана!").ConfigureAwait(false);
				} else {
					await SendTextMessageAsync(message.Chat, "Не получилось создать раздачу, попробуйте ещё раз!").ConfigureAwait(false);
				}
				return;
			case "GIVEAWAYS": {
				var userGiveaways = Giveaways.Where(giveaway => giveaway.Value.OwnerID == message.Chat.Id).ToList();
				await SendTextMessageAsync(message.Chat,
						userGiveaways.Count > 0
							? $"Ваши раздачи:\n{string.Join('\n', userGiveaways.Select(giveaway => giveaway.Value.Title ?? giveaway.Key + ": " + giveaway.Value.AvailableKeys + " ключей"))}"
							: "У вас нет активных раздач!")
					.ConfigureAwait(false);
				return;
			}
		}
	}

	private static string RandomString(int length) {
		const string chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
		return new string(Enumerable.Repeat(chars, length).Select(s => s[Random.Next(s.Length)]).ToArray());
	}

	private static string NameUserRetrieve(User user) =>
		string.IsNullOrEmpty(user.Username) ? user.FirstName + (string.IsNullOrEmpty(user.LastName) ? "" : " " + user.LastName) : "@" + user.Username;

	private static async Task<Message> WaitForMessage(long userID) {
		if (WaitMessageStatuses.ContainsKey(userID)) {
			return null;
		}

		using SemaphoreSlim semaphore = new(0, 1);
		WaitMessageStatuses.Add(userID, (semaphore, null));
		await semaphore.WaitAsync(TimeSpan.FromMinutes(3)).ConfigureAwait(false);
		var message = WaitMessageStatuses[userID].ResultMessage;

		WaitMessageStatuses.Remove(userID);
		return message;
	}

	private static void InBackground(Action action) {
		if (action == null) {
			return;
		}

		Task.Factory.StartNew(action, CancellationToken.None, TaskCreationOptions.DenyChildAttach, TaskScheduler.Default);
	}

	internal static async Task AnswerCallbackQueryAsync(string callbackQueryId, string text = null, bool showAlert = false) {
		try {
			await BotClient.AnswerCallbackQueryAsync(callbackQueryId, text, showAlert).ConfigureAwait(false);
		} catch (ApiRequestException) {
			// ignored
		}
	}

	internal static async Task EditMessageTextAsync(string inlineMessageId, string text, InlineKeyboardMarkup replyMarkup, bool forced = false) {
		// TODO: track inline messages
		if (!forced && (GroupSendMessageSemaphore.CurrentCount == 0)) {
			return;
		}

		await GroupSendMessageSemaphore.WaitAsync().ConfigureAwait(false);
		await SendMessageSemaphore.WaitAsync().ConfigureAwait(false);

		try {
			await BotClient.EditMessageTextAsync(inlineMessageId, text, replyMarkup: replyMarkup).ConfigureAwait(false);
		} finally {
			InBackground(async () => {
				await Task.Delay(50).ConfigureAwait(false);
				SendMessageSemaphore.Release();
			});

			InBackground(async () => {
				await Task.Delay(TimeSpan.FromSeconds(3)).ConfigureAwait(false);
				GroupSendMessageSemaphore.Release();
			});
		}
	}

	internal static Task SendTextMessageAsync(Chat chat, string text, ParseMode parseMode = ParseMode.Markdown) => SendTextMessageAsync(chat.Id, text, parseMode, chat.Type != ChatType.Private);

	private static async Task SendTextMessageAsync(ChatId chatId, string text, ParseMode parseMode = ParseMode.Markdown, bool isGroup = false) {
		if (isGroup) {
			await GroupSendMessageSemaphore.WaitAsync().ConfigureAwait(false);
		}

		await SendMessageSemaphore.WaitAsync().ConfigureAwait(false);

		try {
			await BotClient.SendTextMessageAsync(chatId, text, parseMode: parseMode).ConfigureAwait(false);
		} finally {
			InBackground(async () => {
				await Task.Delay(50).ConfigureAwait(false);
				SendMessageSemaphore.Release();
			});

			if (isGroup) {
				InBackground(async () => {
					await Task.Delay(TimeSpan.FromSeconds(3)).ConfigureAwait(false);
					GroupSendMessageSemaphore.Release();
				});
			}
		}
	}
}
