using System.Collections.Generic;
using System.Text.Json.Serialization;
using System.Threading;

namespace TelegramKeyGiveawayBot {
	internal class GiveawayInfo {
		internal SemaphoreSlim GiveawaySemaphore { get; } = new(1, 1);

		public HashSet<string> AvailableKeys { get; }

		public long OwnerID { get; }

		public HashSet<long> ParticipatedUsers { get; }

		public string Title { get; }

		public string MessageInlineID { get; internal set; }

		internal GiveawayInfo(HashSet<string> availableKeys, long ownerID, string title = null) {
			AvailableKeys = availableKeys;
			OwnerID = ownerID;
			ParticipatedUsers = new HashSet<long>();
			Title = title;
		}

		[JsonConstructor]
		public GiveawayInfo(HashSet<string> availableKeys, string messageInlineID, long ownerID, HashSet<long> participatedUsers, string title) {
			AvailableKeys = availableKeys;
			MessageInlineID = messageInlineID;
			OwnerID = ownerID;
			ParticipatedUsers = participatedUsers;
			Title = title;
		}
	}
}
