using System;
using System.IO;
using System.Runtime.CompilerServices;

namespace TelegramKeyGiveawayBot {
	internal static class Logger {
		// ReSharper disable once ConvertToConstant.Local
#if DEBUG
		private static readonly bool Debugging = true;
#else
		private static readonly bool Debugging = false;
#endif

		private static readonly object LockFile = new();

		internal static void Info(string text, [CallerMemberName] string method = null) {
			Log(text, Level.Info, method);
		}

		internal static void Error(string text, [CallerMemberName] string method = null) {
			Log(text, Level.Error, method);
		}

		internal static void Debug(string text, [CallerMemberName] string method = null) {
			Log(text, Level.Debug, method);
		}

		private static void Log(string text, Level level, string method) {
			var stringToLog = $"[{DateTime.Now:dd/MM/yyyy HH:mm:ss.fff}]|{Enum.GetName(typeof(Level), level).ToUpperInvariant()}|{method}|{text}";
			switch (level) {
				case Level.Info:
					Console.ForegroundColor = ConsoleColor.White;
					break;
				case Level.Error:
					Console.ForegroundColor = ConsoleColor.Red;
					break;
				case Level.Debug:
					Console.ForegroundColor = ConsoleColor.Gray;
					break;
			}

			if ((level != Level.Debug) || Debugging) {
				Console.WriteLine(stringToLog);
			}

			lock (LockFile) {
				File.AppendAllText("log.txt", stringToLog + Environment.NewLine);
			}
		}

		private enum Level {
			Info,
			Error,
			Debug
		}
	}
}
