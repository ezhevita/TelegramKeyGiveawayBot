using System.Collections.Generic;
using System.Linq;

namespace TelegramKeyGiveawayBot {
	internal static class ExtensionMethods {
		internal static T GetRandomValue<T>(this ICollection<T> collection) {
			if ((collection == null) || (collection.Count == 0)) {
				return default;
			}

			return collection.ElementAt(Program.Random.Next(collection.Count));
		}
	}
}
